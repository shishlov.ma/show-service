Для локального разворачивания можно использовать sail - выполнить команду "sail up"

Спецификация находится в файле openapi.yaml

Для подмены шлюза необходимо будет:
- Реализовать SDK для шлюза
- Сервис {Source}ShowGatewayService (например NikitchenkoShowGatewayService)
- Покрытие тестами
- Bind нового сервиса в AppServiceProvider
