<?php
declare(strict_types=1);

namespace App\Exceptions\Services\ShowGatewayService;

use App\Http\Resources;
use Illuminate\Http\JsonResponse;

abstract class BaseException extends \Exception
{
    public function render(): JsonResponse
    {
        $resource = new Resources\ErrorResource('Ошибка при обращении к Gateway Service');

        return response()->json($resource->toArray());
    }
}
