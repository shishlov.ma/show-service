<?php
declare(strict_types=1);

namespace App\Exceptions\Services\ShowGatewayService\NikitchenkoShowGatewayService;

use App\Exceptions\Services\ShowGatewayService\BaseException;

final class ReservationEventPlacesFailedExceptionException extends BaseException
{
    //
}
