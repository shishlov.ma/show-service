<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Resources;
use App\ShowService\DTO;
use Illuminate\Support\Arr;
use Illuminate\Http\JsonResponse;
use App\Exceptions\Services\ShowGatewayService as Exceptions;
use App\ShowService\Contracts\Services\ShowGatewayServiceContract;

final class EventController extends Controller
{
    public function __construct(
        private readonly ShowGatewayServiceContract $service
    )
    {
        //
    }

    /**
     * @throws Exceptions\ShowsException
     * @throws Exceptions\EventsByShowIdException
     * @throws Exceptions\PlacesByEventIdException
     */
    public function show(int $eventId): JsonResponse
    {
        $event = null;

        foreach ($this->service->shows() as $show) {
            $event = Arr::first($this->service->eventsByShowId($show->id), fn(DTO\EventDTO $event) => $event->id === $eventId);

            if (!is_null($event)) {
                break;
            }
        }

        if (is_null($event)) {
            $resource = new Resources\ErrorResource(sprintf('Событие с идентификатором %d не найдено', $eventId));

            return response()->json($resource->toArray());
        }

        $resource = new Resources\Event\ShowResource($event, $this->service->placesByEventId($eventId));

        return response()->json($resource->toArray());
    }

    /**
     * @throws Exceptions\ReserveEventPlacesException
     */
    public function reservePlaces(Requests\Event\ReservePlacesRequest $request, int $eventId): JsonResponse
    {
        $reservationId = $this->service->reserveEventPlaces($eventId, $request->input('name'), $request->input('places'));

        $resource = new Resources\Event\ReservePlacesResource($reservationId);

        return response()->json($resource->toArray());
    }
}
