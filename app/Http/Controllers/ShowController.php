<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources;
use App\ShowService\DTO;
use Illuminate\Support\Arr;
use Illuminate\Http\JsonResponse;
use App\Exceptions\Services\ShowGatewayService as Exceptions;
use App\ShowService\Contracts\Services\ShowGatewayServiceContract;

final class ShowController extends Controller
{
    public function __construct(
        private readonly ShowGatewayServiceContract $service
    )
    {
        //
    }

    /**
     * @throws Exceptions\ShowsException
     */
    public function index(): JsonResponse
    {
        $shows = $this->service->shows();
        $resource = new Resources\Show\IndexResource($shows);

        return response()->json($resource->toArray());
    }

    /**
     * @throws Exceptions\ShowsException
     * @throws Exceptions\EventsByShowIdException
     */
    public function show(int $showId): JsonResponse
    {
        if (is_null($show = Arr::first($this->service->shows(), fn (DTO\ShowDTO $show) => $show->id === $showId))) {
            $resource = new Resources\ErrorResource(sprintf('Мероприятие с идентификатором %d не найдено', $showId));
        } else {
            $events = $this->service->eventsByShowId($showId);

            $resource = new Resources\Show\ShowResource($show, $events);
        }

        return response()->json($resource->toArray());
    }
}
