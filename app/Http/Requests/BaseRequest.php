<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Http\Resources;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class BaseRequest extends FormRequest
{
    protected function failedValidation(Validator $validator): void
    {
        $resource = new Resources\ErrorResource($validator->errors()->first());

        throw new HttpResponseException(
            response()->json($resource->toArray())
        );
    }
}
