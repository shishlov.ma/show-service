<?php
declare(strict_types=1);

namespace App\Http\Requests\Event;

use App\Http\Requests\BaseRequest;

final class ReservePlacesRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:1',
            'places' => 'required|array|min:1',
            'places.*' => 'required|integer',
        ];
    }
}
