<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

final class ErrorResource extends JsonResource
{
    public function __construct(
        private readonly string $errorText = ''
    )
    {
        parent::__construct(null);
    }

    public function toArray(Request $request = null): array
    {
        return [
            'data' => null,
            'error' => true,
            'errorText' => $this->errorText,
        ];
    }
}
