<?php
declare(strict_types=1);

namespace App\Http\Resources\Event;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

final class ReservePlacesResource extends JsonResource
{
    public function __construct(
        private readonly string $reservationId
    )
    {
        parent::__construct(null);
    }

    public function toArray(Request $request = null): array
    {
        return [
            'data' => [
                'reservation_id' => $this->reservationId,
            ],
            'error' => false,
            'errorText' => '',
        ];
    }
}
