<?php
declare(strict_types=1);

namespace App\Http\Resources\Event;

use App\ShowService\DTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

final class ShowResource extends JsonResource
{
    /**
     * @param DTO\PlaceDTO[] $places
     */
    public function __construct(
        private readonly DTO\EventDTO $event,
        private readonly array        $places
    )
    {
        parent::__construct(null);
    }

    public function toArray(Request $request = null): array
    {
        return [
            'data' => [
                'id' => $this->event->id,
                'show_id' => $this->event->showId,
                'date' => $this->event->date,
                'places' => array_map(fn(DTO\PlaceDTO $place) => [
                    'id' => $place->id,
                    'x' => $place->x,
                    'y' => $place->y,
                    'width' => $place->width,
                    'height' => $place->height,
                    'is_available' => $place->isAvailable,
                ], $this->places)
            ],
            'error' => false,
            'errorText' => '',
        ];
    }
}
