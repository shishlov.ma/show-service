<?php
declare(strict_types=1);

namespace App\Http\Resources\Show;

use App\ShowService\DTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

final class IndexResource extends JsonResource
{
    /**
     * @param DTO\ShowDTO[] $shows
     */
    public function __construct(
        private readonly array $shows
    )
    {
        parent::__construct(null);
    }

    public function toArray(Request $request = null): array
    {
        return [
            'data' => array_map(fn(DTO\ShowDTO $show) => [
                'id' => $show->id,
                'name' => $show->name,
            ], $this->shows),
            'error' => false,
            'errorText' => '',
        ];
    }
}
