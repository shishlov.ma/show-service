<?php
declare(strict_types=1);

namespace App\Http\Resources\Show;

use App\ShowService\DTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

final class ShowResource extends JsonResource
{
    /**
     * @param DTO\EventDTO[] $events
     */
    public function __construct(
        private readonly DTO\ShowDTO $show,
        private readonly array       $events
    )
    {
        parent::__construct(null);
    }

    public function toArray(Request $request = null): array
    {
        return [
            'data' => [
                'id' => $this->show->id,
                'name' => $this->show->name,
                'events' => array_map(fn(DTO\EventDTO $event) => [
                    'id' => $event->id,
                    'show_id' => $event->showId,
                    'date' => $event->date,
                ], $this->events),
            ],
            'error' => false,
            'errorText' => '',
        ];
    }
}
