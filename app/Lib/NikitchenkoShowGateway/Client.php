<?php
declare(strict_types=1);

namespace App\Lib\NikitchenkoShowGateway;

use GuzzleHttp\Psr7\MultipartStream;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\Lib\NikitchenkoShowGateway\Contracts\ClientContract;

final readonly class Client implements ClientContract
{
    private GuzzleHttpClient $client;

    public function __construct(
        private string $url,
        private string $token
    )
    {
        $this->client = new GuzzleHttpClient([
            'base_uri' => $this->url,
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
            ],
        ]);
    }

    /**
     * @throws GuzzleException
     */
    public function shows(): ResponseInterface
    {
        return $this->client->get('shows');
    }

    /**
     * @throws GuzzleException
     */
    public function eventsByShowId(int $showId): ResponseInterface
    {
        return $this->client->get(sprintf('shows/%d/events', $showId));
    }

    /**
     * @throws GuzzleException
     */
    public function placesByEventId(int $eventId): ResponseInterface
    {
        return $this->client->get(sprintf('events/%d/places', $eventId));
    }

    /**
     * @param int[] $placeIds
     * @throws GuzzleException
     */
    public function reserveEventPlaces(int $eventId, string $name, array $placeIds): ResponseInterface
    {
        return $this->client->post(sprintf('events/%d/reserve', $eventId), [
            'body' => new MultipartStream($this->transformToMultipartData([
                'name' => $name,
                'places' => $placeIds
            ])),
        ]);
    }

    private function transformToMultipartData(array $body, string $contentName = ''): array
    {
        $result = [];

        foreach ($body as $name => $value) {
            if (is_array($value)) {
                $newContentName = $contentName === '' ? $name : "{$contentName}[$name]";
                $result = array_merge($result, $this->transformToMultipartData($value, $newContentName));
                continue;
            }

            if ($value instanceof \SplFileInfo) {
                $result[] = [
                    'name' => !empty($contentName) ? "{$contentName}[$name]" : $name,
                    'filename' => $value->getFilename(),
                    'contents' => file_get_contents($value->getRealPath()),
                    'headers' => [
                        'content-type' => 'application/octet-stream',
                    ],
                ];
                continue;
            }

            $result[] = [
                'name' => !empty($contentName) ? "{$contentName}[$name]" : $name,
                'contents' => $value,
            ];
        }

        return $result;
    }
}
