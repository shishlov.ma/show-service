<?php
declare(strict_types=1);

namespace App\Lib\NikitchenkoShowGateway;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;

final class ClientServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function boot(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/nikitchenko-show-gateway.php', 'nikitchenko-show-gateway');
    }

    public function register(): void
    {
        $this->app->bind(Contracts\ClientContract::class, function () {
            $url = config('nikitchenko-show-gateway.url');
            $token = config('nikitchenko-show-gateway.token');

            return new Client($url, $token);
        });
    }

    public function provides(): array
    {
        return [
            Contracts\ClientContract::class,
        ];
    }
}
