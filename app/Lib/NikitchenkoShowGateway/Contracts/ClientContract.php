<?php
declare(strict_types=1);

namespace App\Lib\NikitchenkoShowGateway\Contracts;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

interface ClientContract
{
    /**
     * @throws GuzzleException
     */
    public function shows(): ResponseInterface;

    /**
     * @throws GuzzleException
     */
    public function eventsByShowId(int $showId): ResponseInterface;

    /**
     * @throws GuzzleException
     */
    public function placesByEventId(int $eventId): ResponseInterface;

    /**
     * @param int[] $placeIds
     * @throws GuzzleException
     */
    public function reserveEventPlaces(int $eventId, string $name, array $placeIds): ResponseInterface;
}
