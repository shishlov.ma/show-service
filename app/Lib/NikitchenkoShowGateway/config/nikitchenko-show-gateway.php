<?php
declare(strict_types=1);

return [
    'url' => env('NIKITCHENKO_SHOW_GATEWAY_URL', 'https://leadbook.ru/test-task-api/'),
    'token' => env('NIKITCHENKO_SHOW_GATEWAY_TOKEN', ''),
];
