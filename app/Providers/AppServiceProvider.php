<?php
declare(strict_types=1);

namespace App\Providers;

use App\ShowService\Contracts;
use App\ShowService\Services\NikitchenkoShowGatewayService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(Contracts\Services\ShowGatewayServiceContract::class, NikitchenkoShowGatewayService::class);
    }
}
