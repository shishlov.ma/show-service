<?php
declare(strict_types=1);

namespace App\ShowService\Contracts\Services;

use App\ShowService\DTO;
use App\Exceptions\Services\ShowGatewayService as Exceptions;

interface ShowGatewayServiceContract
{
    /**
     * @return DTO\ShowDTO[]
     * @throws Exceptions\ShowsException
     */
    public function shows(): array;

    /**
     * @return DTO\EventDTO[]
     * @throws Exceptions\EventsByShowIdException
     */
    public function eventsByShowId(int $showId): array;

    /**
     * @return DTO\PlaceDTO[]
     * @throws Exceptions\PlacesByEventIdException
     */
    public function placesByEventId(int $eventId): array;

    /**
     * @throws Exceptions\ReserveEventPlacesException
     */
    public function reserveEventPlaces(int $eventId, string $name, array $placeIds): string;
}
