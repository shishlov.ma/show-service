<?php
declare(strict_types=1);

namespace App\ShowService\DTO;

final class EventDTO
{
    public function __construct(
        public int $id,
        public int $showId,
        public string $date
    )
    {
        //
    }
}
