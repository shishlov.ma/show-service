<?php
declare(strict_types=1);

namespace App\ShowService\DTO;

final class PlaceDTO
{
    public function __construct(
        public int $id,
        public float $x,
        public float $y,
        public float $width,
        public float $height,
        public bool $isAvailable
    )
    {
        //
    }
}
