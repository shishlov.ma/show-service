<?php
declare(strict_types=1);

namespace App\ShowService\Services;

use App\ShowService\DTO;
use GuzzleHttp\Exception\GuzzleException;
use App\Lib\NikitchenkoShowGateway\Contracts\ClientContract;
use App\Exceptions\Services\ShowGatewayService as Exceptions;
use App\ShowService\Contracts\Services\ShowGatewayServiceContract;

final readonly class NikitchenkoShowGatewayService implements ShowGatewayServiceContract
{
    public function __construct(
        private ClientContract $client
    )
    {
        //
    }

    /**
     * @return DTO\ShowDTO[]
     * @throws Exceptions\ShowsException
     */
    public function shows(): array
    {
        try {
            $response = $this->client->shows();
            $shows = json_decode($response->getBody()->getContents(), true, flags: JSON_THROW_ON_ERROR)['response'];

            return array_map(fn(array $show): DTO\ShowDTO => new DTO\ShowDTO($show['id'], $show['name']), $shows);
        } catch (GuzzleException|\JsonException $e) {
            throw new Exceptions\ShowsException(previous: $e);
        }
    }

    /**
     * @return DTO\EventDTO[]
     * @throws Exceptions\EventsByShowIdException
     *
     */
    public function eventsByShowId(int $showId): array
    {
        try {
            $response = $this->client->eventsByShowId($showId);
            $events = json_decode($response->getBody()->getContents(), true, flags: JSON_THROW_ON_ERROR)['response'];

            return array_map(fn(array $event): DTO\EventDTO => new DTO\EventDTO(
                $event['id'],
                $event['showId'],
                $event['date']
            ), $events);
        } catch (GuzzleException|\JsonException $e) {
            throw new Exceptions\EventsByShowIdException(previous: $e);
        }
    }

    /**
     * @return DTO\PlaceDTO[]
     * @throws Exceptions\PlacesByEventIdException
     *
     */
    public function placesByEventId(int $eventId): array
    {
        try {
            $response = $this->client->placesByEventId($eventId);
            $places = json_decode($response->getBody()->getContents(), true, flags: JSON_THROW_ON_ERROR)['response'];

            return array_map(fn(array $place): DTO\PlaceDTO => new DTO\PlaceDTO(
                $place['id'],
                $place['x'],
                $place['y'],
                $place['width'],
                $place['height'],
                $place['is_available']
            ), $places);
        } catch (GuzzleException|\JsonException $e) {
            throw new Exceptions\PlacesByEventIdException(previous: $e);
        }
    }

    /**
     * @throws Exceptions\ReserveEventPlacesException
     */
    public function reserveEventPlaces(int $eventId, string $name, array $placeIds): string
    {
        try {
            $response = $this->client->reserveEventPlaces($eventId, $name, $placeIds);
            $reservationId = json_decode($response->getBody()->getContents(), true, flags: JSON_THROW_ON_ERROR)['response']['reservation_id'] ?? null;
        } catch (GuzzleException|\JsonException $e) {
            throw new Exceptions\ReserveEventPlacesException(previous: $e);
        }

        if (is_null($reservationId)) {
            throw new Exceptions\ReserveEventPlacesException();
        }

        return $reservationId;
    }
}
