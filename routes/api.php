<?php
declare(strict_types=1);

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('shows')->group(function () {
    Route::get('/', [Controllers\ShowController::class, 'index']);

    Route::prefix('/{showId}')->group(function () {
        Route::get('/', [Controllers\ShowController::class, 'show']);
    })->where(['showId' => '[0-9]+']);
});

Route::prefix('events/{eventId}')->group(function () {
    Route::get('/', [Controllers\EventController::class, 'show']);
    Route::post('/reserve_places', [Controllers\EventController::class, 'reservePlaces'])->name('events.reserve_places');
})->where(['eventId' => '[0-9]+']);
