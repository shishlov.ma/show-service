<?php
declare(strict_types=1);

namespace Feature\Event;

use Tests\TestCase;
use Mockery\MockInterface;
use Illuminate\Support\Str;
use App\ShowService\Services;
use GuzzleHttp\Psr7\Response;
use App\ShowService\Contracts;
use App\Lib\NikitchenkoShowGateway;
use GuzzleHttp\Exception as GuzzleExceptions;
use App\Exceptions\Services\ShowGatewayService as Exceptions;

class ReservePlacesTest extends TestCase
{
    /**
     * @dataProvider nikitchenko_show_gateway_excepted_exceptions_provider
     */
    public function test_nikitchenko_show_gateway_error_response_if_exception_throw(string $exceptedException): void
    {
        $mock = $this->mock(NikitchenkoShowGateway\Contracts\ClientContract::class, function (MockInterface $mock) use ($exceptedException) {
            $mock
                ->shouldReceive('reserveEventPlaces')
                ->once()
                ->andThrow($exceptedException);
        });
        $this->app->bind(NikitchenkoShowGateway\Contracts\ClientContract::class, fn() => $mock);
        $this->app->bind(Contracts\Services\ShowGatewayServiceContract::class, Services\NikitchenkoShowGatewayService::class);

        $response = $this->post(
            route('events.reserve_places', ['eventId' => rand(1, 1000000)]),
            [
                'name' => fake()->name(),
                'places' => array_map(fn() => rand(1, 100), range(0, rand(1, 10))),
            ]
        );

        $response->assertStatus(200)
            ->assertJson([
                'data' => null,
                'error' => true,
                'errorText' => 'Ошибка при обращении к Gateway Service'
            ])
            ->assertJsonStructure(['data', 'error', 'errorText']);
    }

    public function test_nikitchenko_show_gateway_return_null_reservation_id(): void
    {
        $mock = $this->mock(NikitchenkoShowGateway\Contracts\ClientContract::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('reserveEventPlaces')
                ->once()
                ->andReturn(new Response(200, body: json_encode([
                    'response' => [
                        'success' => false,
                        'reservation_id' => null,
                    ]
                ])));
        });
        $this->app->bind(NikitchenkoShowGateway\Contracts\ClientContract::class, fn() => $mock);
        $this->app->bind(Contracts\Services\ShowGatewayServiceContract::class, Services\NikitchenkoShowGatewayService::class);

        $response = $this->post(
            route('events.reserve_places', ['eventId' => rand(1, 1000000)]),
            [
                'name' => fake()->name(),
                'places' => array_map(fn() => rand(1, 100), range(0, rand(1, 10))),
            ]
        );

        $response->assertStatus(200)
            ->assertJson([
                'data' => null,
                'error' => true,
                'errorText' => 'Ошибка при обращении к Gateway Service'
            ])
            ->assertJsonStructure(['data', 'error', 'errorText']);
    }

    /**
     * @dataProvider nikitchenko_show_gateway_reservation_ids_provider
     */
    public function test_nikitchenko_show_gateway_return_reservation_id(string $reservationId): void
    {
        $mock = $this->mock(NikitchenkoShowGateway\Contracts\ClientContract::class, function (MockInterface $mock) use ($reservationId) {
            $mock
                ->shouldReceive('reserveEventPlaces')
                ->once()
                ->andReturn(new Response(200, body: json_encode([
                    'response' => [
                        'success' => true,
                        'reservation_id' => $reservationId,
                    ]
                ])));
        });
        $this->app->bind(NikitchenkoShowGateway\Contracts\ClientContract::class, fn() => $mock);
        $this->app->bind(Contracts\Services\ShowGatewayServiceContract::class, Services\NikitchenkoShowGatewayService::class);

        $response = $this->post(
            route('events.reserve_places', ['eventId' => rand(1, 1000000)]),
            [
                'name' => fake()->name(),
                'places' => array_map(fn() => rand(1, 100), range(0, rand(1, 10))),
            ]
        );

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'reservation_id' => $reservationId,
                ],
                'error' => false,
                'errorText' => '',
            ])
            ->assertJsonStructure(['data', 'error', 'errorText']);
    }

    public static function nikitchenko_show_gateway_excepted_exceptions_provider(): array
    {
        return [
            [\JsonException::class],
            [GuzzleExceptions\TransferException::class],
            [Exceptions\NikitchenkoShowGatewayService\ReservationEventPlacesFailedExceptionException::class]
        ];
    }

    public static function nikitchenko_show_gateway_reservation_ids_provider(): array
    {
        return array_map(fn() => [Str::random()], range(0, rand(3, 10)));
    }
}
